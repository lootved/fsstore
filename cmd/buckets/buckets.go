package buckets

import (
	"fmt"
	"log"

	"gitlab.com/lootved/fsclient/config"

	"gitlab.com/lootved/fsstore/tables"

	"github.com/spf13/cobra"
)

func Init() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "buckets",
		Short: "Manage key/value store buckets",
		Long:  "Manage key/value store buckets",
	}
	cmd.AddCommand(listCmd())
	cmd.AddCommand(createCmd())
	cmd.AddCommand(deleteCmd())
	return cmd
}

func listBuckets(cmd *cobra.Command, args []string) {

	prefix := ""
	if len(args) > 0 {
		prefix = args[0]
	}

	s, err := tables.OpenRO(config.DbPath)
	if err != nil {
		log.Fatalln(err)
	}
	//bucket:= []byte("abcde")
	buckets := s.GetBuckets(prefix)
	res := make([]string, len(buckets))
	for i, bucketName := range buckets {
		res[i] = string(bucketName)
	}
	fmt.Println(res)
	s.Close()
}

func createBucket(cmd *cobra.Command, args []string) {
	bucketName := args[0]
	s, err := tables.OpenRW(config.DbPath)
	if err != nil {
		log.Fatalln(err)
	}
	s.CreateBucket(bucketName)
	s.Close()
}

func deleteBucket(cmd *cobra.Command, args []string) {
	bucketName := args[0]
	s, err := tables.OpenRW(config.DbPath)
	if err != nil {
		log.Fatalln(err)
	}
	err = s.DeleteBucket(bucketName)
	if err != nil {
		fmt.Println(err)
	}
	s.Close()
}

func deleteCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "delete bucket_name",
		Short: "Delete a bucket if it exists",
		Run:   deleteBucket,
		Args:  cobra.MinimumNArgs(1),
	}
}

func createCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "create",
		Short: "Create a bucket if it doesn't exist",
		Run:   createBucket,
		Args:  cobra.MinimumNArgs(1),
	}
}

func listCmd() *cobra.Command {
	return &cobra.Command{
		Use:                   "ls  [prefix]",
		Short:                 "List available buckets",
		Run:                   listBuckets,
		DisableFlagsInUseLine: false,
		DisableSuggestions:    false,
	}
}
