package config

import "github.com/spf13/viper"

func InitViper() error {
	viper.SetConfigName(".fsstore")
	viper.SetConfigType("json")
	viper.AddConfigPath(".")
	return viper.ReadInConfig()
}
