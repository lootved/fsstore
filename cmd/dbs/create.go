package dbs

import (
	"errors"
	"fmt"
	"log"
	"os"

	"gitlab.com/lootved/fsclient/config"

	"gitlab.com/lootved/fsstore/tables"

	"github.com/spf13/cobra"
)

func createDb(cmd *cobra.Command, args []string) {

	if err := os.MkdirAll(config.DbPath, 0700); err != nil {
		log.Fatalln("Unable to create folder ", config.DbPath)
	}

	for _, dbname := range args {
		dbPath := config.DbPath + "/" + dbname + ".db"
		if _, err := os.Stat(dbPath); !shouldOverride && !errors.Is(err, os.ErrNotExist) {
			fmt.Println("file already exists, use --overwrite flag to overwrite")
			continue
		}

		store, err := tables.OpenRW(dbPath)
		if err != nil {
			fmt.Println("Unable to create db ", dbname)
			continue
		}
		store.Close()
	}
}

func createCmd() *cobra.Command {
	cmd := cobra.Command{
		Use:                   "create dbname [dbnames...]",
		Short:                 "Create a new database",
		Args:                  cobra.MinimumNArgs(1),
		DisableFlagsInUseLine: true,
		Run:                   createDb,
	}
	cmd.Flags().BoolVar(&shouldOverride, "overwrite", false, "overwrite file if it exists")
	return &cmd
}

var shouldOverride bool
