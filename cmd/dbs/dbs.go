package dbs

import "github.com/spf13/cobra"

func Init() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "dbs",
		Short: "Manage databases",
		Long:  "Manage databases",
	}
	cmd.AddCommand(createCmd())
	return cmd
}
