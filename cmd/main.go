package main

import (
	"log"
	"os"

	"gitlab.com/lootved/fsclient/buckets"
	cfg "gitlab.com/lootved/fsclient/config"
	"gitlab.com/lootved/fsclient/dbs"
	"gitlab.com/lootved/fsclient/tables"

	"github.com/spf13/cobra"
)

var (
	cmd = cobra.Command{
		Use:   "fsclient",
		Short: "cli to manage fsstores",
		Long:  "Command line tool to manage fsstore tables",
		//	Run: func(cmd *cobra.Command, args []string) { },
	}
)

func init() {
	//not working
	//cfg.InitViper()

	cmd.AddCommand(tables.Init())
	cmd.AddCommand(buckets.Init())

	cmd.AddCommand(dbs.Init())

	cmd.PersistentFlags().StringVarP(&cfg.ConfigPath, "config", "c", "./.fsstore", "path to config file")

	const defaultPath = "/opt/tmp/datastore"

	cmd.PersistentFlags().StringVarP(&cfg.DbPath, "path", "p", defaultPath, "path to the db to manage")
	//viper.BindPFlags(cmd.PersistentFlags())
}

func main() {

	if err := cmd.Execute(); err != nil {
		log.Println(err)
		os.Exit(1)
	}
}
