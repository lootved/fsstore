package tables

import (
	"log"
	"time"

	"gitlab.com/lootved/fsclient/config"

	"gitlab.com/lootved/fsstore/tables"

	"github.com/spf13/cobra"
)

type sessionsCmd struct{}

func (scmd sessionsCmd) get(s *tables.Store, keys []string) interface{} {
	sessions, err := s.GetSessions(keys)
	if err != nil {
		log.Fatalln(err)
	}
	return sessions
}

func (scmd sessionsCmd) rm(s *tables.Store, keys []string) {
	s.DeleteSessions(keys)
}

func (scmd sessionsCmd) set(s *tables.Store, args []string) error {
	key := args[0]
	uid := args[1]

	return s.PutSession(key, uid)
}
func (scmd sessionsCmd) lsk(s *tables.Store) []string {
	return s.GetAllSessionkeys()
}

func cleanSessions(tableName string) *cobra.Command {
	return &cobra.Command{
		Use:   "clean",
		Short: "delete any expired sessions",
		Run: func(cmd *cobra.Command, args []string) {
			s, err := tables.OpenRW(config.DbPath + "/" + tableName + ".db")
			if err != nil {
				log.Fatalln(err)
			}
			defer s.Close()

			sessions := s.GetAllSessions()
			var expiredSessions []string
			now := time.Now()
			for sid, session := range sessions {
				if session.ExpirationTime.After(now) {
					expiredSessions = append(expiredSessions, sid)
				}
			}
			s.DeleteSessions(expiredSessions)
		},
	}
}
