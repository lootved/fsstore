package tables

import (
	"fmt"
	"log"

	"gitlab.com/lootved/fsclient/config"

	"gitlab.com/lootved/fsstore/tables"

	"github.com/spf13/cobra"
)

type tableCmd interface {
	get(s *tables.Store, keys []string) interface{}
	set(s *tables.Store, args []string) error
	lsk(s *tables.Store) []string
	rm(s *tables.Store, args []string)
}

func Init() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "tables",
		Short: "Manage key/value store tables",
		Long:  "Manage a collection of buckets with the same prefix (5 char minimun)",
	}

	cmd.AddCommand(tableListCmd())

	cmd.AddCommand(makeTableCmd("users", "username password", usersCmd{}))

	sessionsTableCmd := makeTableCmd("sessions", "key uid", sessionsCmd{})
	sessionsTableCmd.AddCommand(cleanSessions("sessions"))
	cmd.AddCommand(sessionsTableCmd)

	return cmd
}

func tableListCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "ls",
		Short: "try to list all tables, not accurate ",
		Run: func(cmd *cobra.Command, args []string) {
			s, err := tables.OpenRO(config.DbPath)
			if err != nil {
				log.Fatalln(err)
			}
			res := make(map[string]bool)
			for _, bucket := range s.ListBuckets() {
				res[string(bucket[:5])] = true
			}
			for k := range res {
				fmt.Print(k, " ")
			}
			s.Close()
		},
	}
}

func makeTableCmd(tableName string, useSuffix string, tcmd tableCmd) *cobra.Command {
	cmd := &cobra.Command{
		Use:   tableName,
		Short: "Manage " + tableName + " table",
		Long:  "Manage all buckets that are prefixed by " + tableName,
	}
	cmd.AddCommand(setCmd(tableName, tcmd, useSuffix))
	cmd.AddCommand(getCmd(tableName, tcmd))
	cmd.AddCommand(lskCmd(tableName, tcmd))
	cmd.AddCommand(rmCmd(tableName, tcmd))
	return cmd
}

func getCmd(tableName string, tcmd tableCmd) *cobra.Command {
	size := len(tableName)
	return &cobra.Command{
		Use:   "get " + tableName[:size-1] + "_key [...keys]",
		Short: "retrieve " + tableName + " by key ",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {

			s, err := tables.OpenRO(config.DbPath + "/" + tableName + ".db")
			if err != nil {
				log.Fatalln(err)
			}
			defer s.Close()
			val := tcmd.get(s, args)
			fmt.Println(val)
		},
	}
}

func setCmd(tableName string, tcmd tableCmd, useSuffix string) *cobra.Command {
	size := len(tableName)
	return &cobra.Command{
		Use:   "set " + useSuffix,
		Short: "create or update a " + tableName[:size-1],
		Args:  cobra.MinimumNArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			s, err := tables.OpenRW(config.DbPath + "/" + tableName + ".db")
			if err != nil {
				log.Fatalln(err)
			}
			tcmd.set(s, args)
			s.Close()
		},
	}
}

func lskCmd(tableName string, tcmd tableCmd) *cobra.Command {
	return &cobra.Command{
		Use:   "lsk",
		Short: "list all the " + tableName + " keys ",
		Run: func(cmd *cobra.Command, args []string) {

			s, err := tables.OpenRO(config.DbPath + "/" + tableName + ".db")
			if err != nil {
				log.Fatalln(err)
			}
			keys := tcmd.lsk(s)
			fmt.Println(keys)
			s.Close()
		},
	}
}

func rmCmd(tableName string, tcmd tableCmd) *cobra.Command {
	size := len(tableName)
	return &cobra.Command{
		Use:   "rm " + tableName[:size-1] + "_key [...keys]",
		Short: "delete keys fom " + tableName + " if present",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {

			s, err := tables.OpenRW(config.DbPath + "/" + tableName + ".db")
			if err != nil {
				log.Fatalln(err)
			}
			defer s.Close()
			tcmd.rm(s, args)
		},
	}
}
