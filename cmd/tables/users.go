package tables

import (
	"fmt"
	"os"

	"gitlab.com/lootved/fsstore/hash"
	"gitlab.com/lootved/fsstore/models"
	"gitlab.com/lootved/fsstore/tables"
)

type usersCmd struct{}

func (scmd usersCmd) get(s *tables.Store, keys []string) interface{} {
	user, err := s.GetUsers(keys)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return user
}

func (scmd usersCmd) rm(s *tables.Store, keys []string) {
	s.DeleteUsers(keys)
}

func (scmd usersCmd) set(s *tables.Store, args []string) error {
	key := hash.HashUser(args[0], args[1])
	user := models.User{Name: args[0]}
	return s.PutUser(key, user)
}
func (scmd usersCmd) lsk(s *tables.Store) []string {
	return s.GetAllUserKeys()
}
