module gitlab.com/lootved/fsstore

go 1.18

require go.etcd.io/bbolt v1.3.6

require golang.org/x/sys v0.0.0-20220412211240-33da011f77ad // indirect
