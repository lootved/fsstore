package hash

import (
	"crypto/md5"
	b64 "encoding/base64"
	"encoding/hex"
	"math/rand"
	"time"
)

func HashUser(username string, pass string) string {
	basic := b64.StdEncoding.EncodeToString([]byte(username + ":" + pass))
	return Sum("Basic " + basic)
}

func Sum(authorization string) string {
	sum := md5.Sum([]byte("useless" + authorization + "salt"))
	return hex.EncodeToString(sum[:])
}

var seed *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

const charset = "abcdefghijklmnopqrstuvwxyz" + "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

func RandomUid(length int) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seed.Intn(len(charset))]
	}
	return string(b)
}
