package models

import "time"

type Session struct {
	ExpirationTime time.Time
	UserId         string
}
