package models

type User struct {
	Name  string
	Roles []string
}
