package tables

import (
	"bytes"
	"encoding/gob"
	"time"

	"gitlab.com/lootved/fsstore/models"

	"go.etcd.io/bbolt"
)

const SESSIONS_TABLE = "sessions"

func UpsertSessions(path string, sessions map[string]models.Session) error {
	s, err := OpenRW(path)
	if err != nil {
		return err
	}
	defer s.Close()
	return putMany(SESSIONS_TABLE, sessions, s)
}

func (s *Store) UpsertSessions(sessionIdToUid map[string]string) error {
	sessions := make(map[string]models.Session)
	for key := range sessionIdToUid {
		sessions[key] = models.Session{UserId: sessionIdToUid[key], ExpirationTime: time.Now()}
	}
	return putMany(SESSIONS_TABLE, sessions, s)
}

func (s *Store) UpsertSession(sessionId string, uid string) error {
	sessions := make(map[string]string)
	return s.UpsertSessions(sessions)
}

func (s *Store) GetAllSessionkeys() []string {
	return s.getAllKeys(SESSIONS_TABLE)
}

func (s *Store) GetAllSessions() map[string]models.Session {
	res := make(map[string]models.Session)
	bPrefix := []byte(SESSIONS_TABLE)
	var session models.Session
	s.db.View(func(tx *bbolt.Tx) error {
		return tx.ForEach(func(name []byte, b *bbolt.Bucket) error {
			var err error
			if bytes.HasPrefix(name, bPrefix) {
				return b.ForEach(func(k, v []byte) error {
					d := gob.NewDecoder(bytes.NewReader(v))
					err = d.Decode(&session)
					res[string(k)] = session
					return err
				})
			}
			return err
		})
	})
	return res
}

func (s *Store) GetSessions(keys []string) ([]models.Session, error) {
	sessions := make([]models.Session, 0)
	return get(SESSIONS_TABLE, keys, s.db, sessions)
}

func (s Store) GetSession(key string) (*models.Session, error) {
	var val models.Session
	err := s.get(SESSIONS_TABLE, key, &val)
	return &val, err
}

func (s Store) PutSession(key string, uid string) error {
	val := models.Session{UserId: uid, ExpirationTime: time.Now()}
	err := s.put(SESSIONS_TABLE, key, &val)
	return err
}

func (s Store) DeleteSessions(keys []string) error {
	return s.delete(SESSIONS_TABLE, keys)
}
