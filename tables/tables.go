package tables

import (
	"errors"
	"sort"

	"gitlab.com/lootved/fsstore/models"

	"bytes"
	"encoding/gob"

	"go.etcd.io/bbolt"
)

type Store struct {
	db *bbolt.DB
}

var (
	ErrBadValue error = errors.New("nil value supplied to store")
	ErrNotFound error = errors.New("value not found")
)

func OpenRO(path string) (*Store, error) {
	db, err := bbolt.Open(path, 0400, nil)
	if err != nil {
		return nil, err
	}
	return &Store{db}, nil
}

func OpenRW(path string) (*Store, error) {
	db, err := bbolt.Open(path, 0700, nil)
	if err != nil {
		return nil, err
	}

	return &Store{db}, nil
}

func keyToBucket(table string, key string) []byte {
	return []byte(table + key[:1])
}

func (s *Store) put(table string, key string, value interface{}) error {
	if value == nil {
		return ErrBadValue
	}
	var buf bytes.Buffer
	if err := gob.NewEncoder(&buf).Encode(value); err != nil {
		return err
	}
	bucket := keyToBucket(table, key)
	return s.db.Update(func(tx *bbolt.Tx) error {
		if _, err := tx.CreateBucketIfNotExists(bucket); err != nil {
			return err
		}
		return tx.Bucket(bucket).Put([]byte(key), buf.Bytes())
	})
}

func get[T models.Any](table string, keys []string, db *bbolt.DB, output []T) ([]T, error) {
	sort.Strings(keys)
	err := db.View(func(tx *bbolt.Tx) error {
		var err error
		var buf T
		for _, key := range keys {
			bucketName := keyToBucket(table, key)
			bucket := tx.Bucket(bucketName)
			if bucket == nil {
				continue
			}
			c := bucket.Cursor()
			k, v := c.Seek([]byte(key))
			//log.Println("retrieving", key, " from bucket", string(bucket))
			if string(k) == key {
				d := gob.NewDecoder(bytes.NewReader(v))
				err = d.Decode(&buf)
				output = append(output, buf)
			}
		}
		return err
	})
	return output, err
}

func (s *Store) get(table string, key string, value interface{}) error {
	if value == nil {
		return ErrBadValue
	}
	bucket := keyToBucket(table, key)
	//	log.Println("using bucket ", string(bucket))

	return s.db.View(func(tx *bbolt.Tx) error {
		c := tx.Bucket(bucket).Cursor()
		k, v := c.Seek([]byte(key))
		//log.Println("retrieving", key, " from bucket", string(bucket))
		if k == nil || string(k) != key {
			return ErrNotFound
		}
		d := gob.NewDecoder(bytes.NewReader(v))
		return d.Decode(value)
	})
}

func putOne(key string, table string, value interface{}, path string) error {
	s, err := OpenRW(path)
	if err != nil {
		return err
	}
	defer s.Close()
	return s.put(table, key, value)
}

func getOne(key string, table string, value interface{}, path string) error {
	s, err := OpenRO(path)
	if err != nil {
		return err
	}
	defer s.Close()
	return s.get(table, key, value)
}

func (s *Store) Close() error {
	return s.db.Close()
}

func (s *Store) BackupToFile(path string) error {
	return s.db.View(func(tx *bbolt.Tx) error {
		return tx.CopyFile(path, 0600)
	})
}

func (s *Store) getAllKeys(table string) []string {
	var res []string
	bPrefix := []byte(table)
	s.db.View(func(tx *bbolt.Tx) error {
		return tx.ForEach(func(name []byte, b *bbolt.Bucket) error {
			if bytes.HasPrefix(name, bPrefix) {
				return b.ForEach(func(k, v []byte) error {
					res = append(res, string(k))
					return nil
				})
			}
			return nil
		})
	})
	return res
}

func (s *Store) GetBuckets(prefix string) [][]byte {
	var bucketNames [][]byte
	bPrefix := []byte(prefix)

	s.db.View(func(tx *bbolt.Tx) error {
		tx.ForEach(func(name []byte, b *bbolt.Bucket) error {
			if bytes.HasPrefix(name, bPrefix) {
				bucketNames = append(bucketNames, name)
			}
			return nil
		})
		return nil
	})
	return bucketNames
}

func (s *Store) ListBuckets() [][]byte {
	return s.GetBuckets("")
}

func (s *Store) CreateBucket(bucket string) error {
	return s.db.Update(func(tx *bbolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(bucket))
		return err
	})
}

func (s *Store) DeleteBucket(bucket string) error {
	return s.db.Update(func(tx *bbolt.Tx) error {
		return tx.DeleteBucket([]byte(bucket))
	})
}

func putMany[T models.Any](table string, values map[string]T, s *Store) error {
	var buf bytes.Buffer
	return s.db.Update(func(tx *bbolt.Tx) error {
		var err error
		for key := range values {
			bucket := keyToBucket(table, key)
			gob.NewEncoder(&buf).Encode(values[key])
			tx.CreateBucketIfNotExists(bucket)
			//log.Println("saving", key, " in bucket", string(bucket))
			err = tx.Bucket(bucket).Put([]byte(key), buf.Bytes())
		}
		return err
	})
}

func (s *Store) delete(table string, keys []string) error {
	sort.Strings(keys)
	return s.db.Update(func(tx *bbolt.Tx) error {
		for _, key := range keys {
			bucketName := keyToBucket(table, key)
			bucket := tx.Bucket(bucketName)
			if bucket == nil {
				continue
			}
			c := bucket.Cursor()
			k, _ := c.Seek([]byte(key))
			if string(k) == key {
				//log.Println("Deleting", string(k), "from ", string(bucketName))
				c.Delete()
			}
		}
		return nil
	})
}
