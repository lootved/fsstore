package tables

import (
	"gitlab.com/lootved/fsstore/models"
)

const USERS_TABLE = "users"

func (s *Store) GetAllUserKeys() []string {
	return s.getAllKeys(USERS_TABLE)
}

func (s *Store) UpsertUsers(users map[string]models.User) error {
	return putMany(USERS_TABLE, users, s)
}

func (s *Store) GetUsers(keys []string) ([]models.User, error) {
	var users []models.User
	return get(USERS_TABLE, keys, s.db, users)
}

func (s *Store) GetUser(key string) (*models.User, error) {
	var val models.User
	err := s.get(USERS_TABLE, key, &val)
	return &val, err
}

func (s *Store) PutUser(key string, val models.User) error {
	err := s.put(USERS_TABLE, key, &val)
	return err
}

func (s *Store) DeleteUsers(keys []string) {
	s.delete(USERS_TABLE, keys)
}
